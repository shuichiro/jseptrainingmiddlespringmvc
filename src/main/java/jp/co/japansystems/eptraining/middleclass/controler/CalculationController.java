package jp.co.japansystems.eptraining.middleclass.controler;


import static org.springframework.web.bind.annotation.RequestMethod.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.japansystems.eptraining.middleclass.logic.CalculationLogic;

@Controller
public class CalculationController {

	@Autowired
	private CalculationLogic logic;

    @RequestMapping(value = "/initial", method = GET)
    public String initial(Model model) {

        return "input";
    }

    //ここにメソッドを実装する

}