package jp.co.japansystems.eptraining.middleclass.form;

public class CalculationForm {

	private long param1;
	private long param2;

	private long answer;

	public long getParam1() {
		return param1;
	}

	public void setParam1(long param1) {
		this.param1 = param1;
	}

	public long getParam2() {
		return param2;
	}

	public void setParam2(long param2) {
		this.param2 = param2;
	}

	public long getAnswer() {
		return answer;
	}

	public void setAnswer(long answer) {
		this.answer = answer;
	}

	@Override
	public boolean equals(Object obj) {
		CalculationForm form = (CalculationForm) obj;
		boolean flag = true;
		flag = flag && this.param1 == form.getParam1() ? true : false;
		flag = flag && this.param2 == form.getParam2() ? true : false;
		flag = flag && this.answer == form.getAnswer() ? true : false;
		return flag;
	}

}
