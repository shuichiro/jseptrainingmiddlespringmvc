package jp.co.japansystems.eptraining.middleclass.form;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CalculationFormTest  {

	CalculationForm target;


	@Before
	public void setUp() throws Exception {
		target = new CalculationForm();
	}


	@After
	public void tearDown() throws Exception {
		target = null;
	}

	@Test
	public void testGetParam1() {

		target.setParam1(30);
		assertEquals(30, target.getParam1());
	}

	@Test
	public void testSetParam1() {
		target.setParam1(30);
		assertEquals(30, target.getParam1());
	}

	@Test
	public void testGetParam2() {
		target.setParam2(20);
		assertEquals(20, target.getParam2());
	}

	@Test
	public void testSetParam2() {
		target.setParam2(20);
		assertEquals(20, target.getParam2());
	}

	@Test
	public void testGetAnswer() {
		target.setAnswer(10);
		assertEquals(10, target.getAnswer());
	}

	@Test
	public void testSetAnswer() {
		target.setAnswer(10);
		assertEquals(10, target.getAnswer());
	}

	@Test
	public void testEquals1() {
		CalculationForm form1 = new CalculationForm();
		form1.setParam1(1);
		form1.setParam2(2);
		form1.setAnswer(3);


		CalculationForm form2 = new CalculationForm();

		form2.setParam1(1);
		form2.setParam2(2);
		form2.setAnswer(3);

		assertEquals(true, form1.equals(form2));

	}

	@Test
	public void testEquals2() {
		CalculationForm form1 = new CalculationForm();
		form1.setParam1(4);
		form1.setParam2(2);
		form1.setAnswer(3);


		CalculationForm form2 = new CalculationForm();

		form2.setParam1(1);
		form2.setParam2(2);
		form2.setAnswer(3);

		assertEquals(false, form1.equals(form2));

	}


	@Test
	public void testEquals3() {
		CalculationForm form1 = new CalculationForm();
		form1.setParam1(1);
		form1.setParam2(4);
		form1.setAnswer(3);


		CalculationForm form2 = new CalculationForm();

		form2.setParam1(1);
		form2.setParam2(2);
		form2.setAnswer(3);

		assertEquals(false, form1.equals(form2));

	}
	@Test
	public void testEquals4() {
		CalculationForm form1 = new CalculationForm();
		form1.setParam1(1);
		form1.setParam2(2);
		form1.setAnswer(4);


		CalculationForm form2 = new CalculationForm();

		form2.setParam1(1);
		form2.setParam2(2);
		form2.setAnswer(3);

		assertEquals(false, form1.equals(form2));

	}


}
