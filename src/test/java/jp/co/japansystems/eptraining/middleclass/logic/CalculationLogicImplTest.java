package jp.co.japansystems.eptraining.middleclass.logic;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/dispatcher-servlet.xml" })
@WebAppConfiguration
public class CalculationLogicImplTest {

	@Autowired
	CalculationLogic target;

	@Test
	public void testCalcPlus() {
		long result = target.calcPlus(1,2);
		assertEquals(3, result);
	}

	@Test
	public void testCalcMinus() {
		long result = target.calcMinus(4,3);
		assertEquals(1, result);
	}

	@Test
	public void testCalcMultiply() {
		long result = target.calcMultiply(5,6);
		assertEquals(30, result);
	}

	@Test
	public void testCalcDevide1() {
		long result = target.calcDevide(14,7);
		assertEquals(2, result);
	}

	@Test
	public void testCalcDevide2() {
		long result = target.calcDevide(14,0);
		assertEquals(0, result);
	}

}
