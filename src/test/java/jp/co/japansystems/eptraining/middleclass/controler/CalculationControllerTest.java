package jp.co.japansystems.eptraining.middleclass.controler;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import jp.co.japansystems.eptraining.middleclass.form.CalculationForm;
import jp.co.japansystems.eptraining.middleclass.logic.CalculationLogic;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/dispatcher-servlet.xml" })
@WebAppConfiguration
public class CalculationControllerTest {

	private MockMvc mvc;

	@InjectMocks
	private CalculationController calculationController;

	@Mock
	private CalculationLogic calculationLogic;

	@Rule
	public final MockitoRule rule = MockitoJUnit.rule();

	@Before
	public void setup() {
		mvc = MockMvcBuilders.standaloneSetup(this.calculationController).build();
	}

	@Test
	public void testInitial() throws Exception {
		mvc.perform(get("/initial")).andExpect(status().isOk()).andExpect(view().name("input"));
	}

	@Test
	public void testCalculatePlus() throws Exception {

		// Controllerの中で使用されているLogicのモックの動作を指定する。
		when(calculationLogic.calcPlus(3, 5)).thenReturn((long) 9);

		// Controllerが”/calculate”で呼ばれたきと同じ動きをするためのメソッド
		// パラメータを指定する
		ResultActions action = mvc
				.perform(post("/calculate").param("plus", "").param("param1", "3").param("param2", "5"))
				// ステータスがOKかどうか確認する
				.andExpect(status().isOk())
				// 次の遷移先Viewがinputかどうか確認する
				.andExpect(view().name("input"))
				// ”calculationForm”がJSPにわたっているか確認する
				.andExpect(model().attributeExists("calculationForm"));

		// 設定してあるAnswerを確認する
		CalculationForm resultForm = (CalculationForm) action.andReturn().getModelAndView().getModel()
				.get("calculationForm");
		assertEquals(9, resultForm.getAnswer());
	}

	@Test
	public void testCalculateMinus() throws Exception {

		// Controllerの中で使用されているLogicのモックの動作を指定する。
		when(calculationLogic.calcMinus(3, 5)).thenReturn((long) 9);

		// Controllerが”/calculate”で呼ばれたきと同じ動きをするためのメソッド
		// パラメータを指定する
		ResultActions action = mvc
				.perform(post("/calculate").param("minus", "").param("param1", "3").param("param2", "5"))
				// ステータスがOKかどうか確認する
				.andExpect(status().isOk())
				// 次の遷移先Viewがinputかどうか確認する
				.andExpect(view().name("input"))
				// ”calculationForm”がJSPにわたっているか確認する
				.andExpect(model().attributeExists("calculationForm"));

		// 設定してあるAnswerを確認する
		CalculationForm resultForm = (CalculationForm) action.andReturn().getModelAndView().getModel()
				.get("calculationForm");
		assertEquals(9, resultForm.getAnswer());
	}

	@Test
	public void testCalculateMultiply() throws Exception {

		// Controllerの中で使用されているLogicのモックの動作を指定する。
		when(calculationLogic.calcMultiply(3, 5)).thenReturn((long) 9);

		// Controllerが”/calculate”で呼ばれたきと同じ動きをするためのメソッド
		// パラメータを指定する
		ResultActions action = mvc
				.perform(post("/calculate").param("multiply", "").param("param1", "3").param("param2", "5"))
				// ステータスがOKかどうか確認する
				.andExpect(status().isOk())
				// 次の遷移先Viewがinputかどうか確認する
				.andExpect(view().name("input"))
				// ”calculationForm”がJSPにわたっているか確認する
				.andExpect(model().attributeExists("calculationForm"));

		// 設定してあるAnswerを確認する
		CalculationForm resultForm = (CalculationForm) action.andReturn().getModelAndView().getModel()
				.get("calculationForm");
		assertEquals(9, resultForm.getAnswer());
	}

	@Test
	public void testCalculateDevide() throws Exception {

		// Controllerの中で使用されているLogicのモックの動作を指定する。
		when(calculationLogic.calcDevide(3, 5)).thenReturn((long) 9);

		// Controllerが”/calculate”で呼ばれたきと同じ動きをするためのメソッド
		// パラメータを指定する
		ResultActions action = mvc
				.perform(post("/calculate").param("devide", "").param("param1", "3").param("param2", "5"))
				// ステータスがOKかどうか確認する
				.andExpect(status().isOk())
				// 次の遷移先Viewがinputかどうか確認する
				.andExpect(view().name("input"))
				// ”calculationForm”がJSPにわたっているか確認する
				.andExpect(model().attributeExists("calculationForm"));

		// 設定してあるAnswerを確認する
		CalculationForm resultForm = (CalculationForm) action.andReturn().getModelAndView().getModel()
				.get("calculationForm");
		assertEquals(9, resultForm.getAnswer());
	}
}
